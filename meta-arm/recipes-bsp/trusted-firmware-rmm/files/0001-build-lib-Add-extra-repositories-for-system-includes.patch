From bc7dbac20a6674eb2834bd6176665f1a2ae42edc Mon Sep 17 00:00:00 2001
From: Mathieu Poirier <mathieu.poirier@linaro.org>
Date: Thu, 14 Mar 2024 14:59:30 -0600
Subject: [PATCH] build(lib): Add extra repositories for system includes

Toolchains such as aarch64-none-elf, aarch64-none-linux-gnu and
aarch64-linux-gnu include assert.h and limits.h in a directory that is
part of their search path.  This is not the case when compiling with
Yocto where aarch64-poky-linux places those files in the sysroot
directory of the component being compiled.

Since the sysroot directory of the component is not part of the cmake
search path, compiling the RMM in Yocto fails.  This patch fixes the
problem by expanding the search path when needed, allowing the RMM to be
compiled in Yocto.

Upstream-Status: Backport [bc7dbac20a6674eb2834bd6176665f1a2ae42edc]
Signed-off-by: Mathieu Poirier <mathieu.poirier@linaro.org>
---
 lib/arch/CMakeLists.txt | 3 +++
 lib/libc/CMakeLists.txt | 5 ++++-
 2 files changed, 7 insertions(+), 1 deletion(-)

diff --git a/lib/arch/CMakeLists.txt b/lib/arch/CMakeLists.txt
index d3afc5f2bfc8..a52185f02695 100644
--- a/lib/arch/CMakeLists.txt
+++ b/lib/arch/CMakeLists.txt
@@ -12,6 +12,9 @@ target_link_libraries(rmm-lib-arch
 target_include_directories(rmm-lib-arch
     PUBLIC  "include"
             "include/${RMM_ARCH}"
+            # The CMAKE_INCLUDE_PATH is included here for Yocto builds.  the
+            # Yocto recipe will define this variable as part of the build.
+            ${CMAKE_INCLUDE_PATH}
     PRIVATE "src/${RMM_ARCH}"
             "src/include")
 
diff --git a/lib/libc/CMakeLists.txt b/lib/libc/CMakeLists.txt
index 1631332dbc72..a2adf37f7cb8 100644
--- a/lib/libc/CMakeLists.txt
+++ b/lib/libc/CMakeLists.txt
@@ -12,7 +12,10 @@ if(NOT RMM_ARCH STREQUAL fake_host)
            rmm-lib-debug)
 
     target_include_directories(rmm-lib-libc SYSTEM
-        PUBLIC "include")
+        PUBLIC "include"
+        # The CMAKE_INCLUDE_PATH is included here for Yocto builds.  the
+        # Yocto recipe will define this variable as part of the build.
+        ${CMAKE_INCLUDE_PATH})
 
     target_sources(rmm-lib-libc
         PRIVATE "src/abort.c"
-- 
2.34.1

