# These machines cannot run in product as they are virtual
# platforms, so allow ssh-pregen-hostkeys to be used.

COMPATIBLE_MACHINE:corestone1000-fvp = "corestone1000-fvp"
COMPATIBLE_MACHINE:fvp-base = "fvp-base"
COMPATIBLE_MACHINE:sbsa-ref = "sbsa-ref"
